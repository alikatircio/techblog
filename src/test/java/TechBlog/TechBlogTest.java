package TechBlog;

import base.TestBase;
import org.testng.annotations.Test;
import pages.TechBlogPage;


public class TechBlogTest extends TestBase {

    TechBlogPage tbp = new TechBlogPage();

    @Test
    public void testClass1 () {

        tbp.checkAutorAndImage(driver);
        tbp.checkBrowserTitleAndNewLink(driver);
    }
}
