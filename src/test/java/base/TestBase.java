package base;

import com.sun.org.apache.xpath.internal.operations.Or;
import javafx.util.Duration;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;


import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;


public class TestBase {

    public static Properties CONFIG = null;
    public static Properties OR = null;
    public static WebDriver driver = null;

    public static void initializeConfigAndObjectRepository() throws IOException {

        CONFIG = new Properties();
        CONFIG.load(new FileInputStream(System.getProperty("user.dir") + "//src//etc//config.properties"));
        OR = new Properties();
        FileInputStream ip_OR = new FileInputStream(System.getProperty("user.dir") + "//src//etc//OR.properties");
        OR.load(ip_OR);
    }

    @BeforeSuite
    public synchronized void beforeSuite(ITestContext context) throws Throwable {

        initializeConfigAndObjectRepository();
    }

    @BeforeClass
    public void BeforeMethod() {
        System.setProperty("webdriver.gecko.driver",
                System.getProperty("user.dir") + "//geckodriver");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.navigate().to(CONFIG.getProperty("url"));
    }

    @AfterClass
    public void AfterMethod() {

        driver.quit();
    }

    public void waitUntilElementClickableWithXpath (WebDriver driver, String xpath) {


        WebDriverWait wait = new WebDriverWait(driver, 15);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OR.getProperty(xpath))));
    }

    public void waitUntilVisibleWithXpath (WebDriver driver, String xpath) {


        WebDriverWait wait = new WebDriverWait(driver, 15);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OR.getProperty(xpath))));
    }

    public void waitUntilVisibleWithStringXpath (WebDriver driver, String xpath) {


        WebDriverWait wait = new WebDriverWait(driver, 15);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
    }

    public WebElement getWebElementWithXpath (WebDriver driver, String xpath) {

        WebElement ele = driver.findElement(By.xpath(OR.getProperty(xpath)));
        return ele;
    }

    public WebElement getWebElementWithStringXpath (WebDriver driver, String xpath) {

        WebElement ele = driver.findElement(By.xpath(xpath));
        return ele;
    }

    public List<WebElement> getListOfWebElementWithStringXpath (WebDriver driver, String xpath) {

        List<WebElement> list = driver.findElements(By.xpath(xpath));
        return list;

    }
}
