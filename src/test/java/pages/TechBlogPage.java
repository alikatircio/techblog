package pages;

import base.TestBase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.List;

public class TechBlogPage extends TestBase {

    public void checkAutorAndImage (WebDriver driver) {

        waitUntilVisibleWithXpath(driver, "theLastNewsTitleXpath");
        if (getWebElementWithXpath(driver, "theLastNewsTitleXpath").getText().equalsIgnoreCase("The Latest")){

            String xpath = OR.getProperty("theLastNewsTitleXpath") + OR.getProperty("eachNewsXpath");
            List<WebElement> list = getListOfWebElementWithStringXpath(driver, xpath);
            for (int i = 1; i <= list.size(); i++){

                xpath = OR.getProperty("theLastNewsTitleXpath") + OR.getProperty("eachNewsXpath")+"["+ i +"]"+OR.getProperty("authorsXpath");
                Assert.assertNotNull(getWebElementWithStringXpath(driver, xpath));
                xpath = OR.getProperty("theLastNewsTitleXpath") + OR.getProperty("eachNewsXpath")+"["+ i +"]"+"//img";
                Assert.assertNotNull(getWebElementWithStringXpath(driver, xpath).getAttribute("src"));
            }

            System.out.println("checkAutorAndImage method finish...");
        }
    }

    public void checkBrowserTitleAndNewLink (WebDriver driver) {

        waitUntilVisibleWithXpath(driver, "theLastNewsTitleXpath");
        if (getWebElementWithXpath(driver, "theLastNewsTitleXpath").getText().equalsIgnoreCase("The Latest")){

            String xpath = OR.getProperty("theLastNewsTitleXpath") + OR.getProperty("eachNewsXpath");
            List<WebElement> list = getListOfWebElementWithStringXpath(driver, xpath);
            for (int i = 1; i <= list.size(); i++){

                xpath = OR.getProperty("theLastNewsTitleXpath") + OR.getProperty("eachNewsXpath")+"["+ i +"]";
                getWebElementWithStringXpath(driver, xpath).click();
                xpath = xpath + "//h1";
                waitUntilVisibleWithStringXpath(driver, xpath);
                String titles[] = driver.getTitle().split("[|]");
                Assert.assertEquals(getWebElementWithStringXpath(driver, xpath).getText(), titles[0].trim());
                String link = driver.getCurrentUrl().replaceAll("[/\\d,/\\s,/\\//;]", "");
                String ll[] = link.split(".com");
                String url[] = ll[1].split("[-]");
                for (int j = 0; j< url.length; j++) {

                   if(getWebElementWithStringXpath(driver, xpath).getText().toLowerCase().contains(url[j].toLowerCase()))
                       System.out.println("yes matched title: "+getWebElementWithStringXpath(driver, xpath).getText()+ " url: " +ll[1] +" "+url[j]);
                   else
                       System.out.println("no not matched title: "+getWebElementWithStringXpath(driver, xpath).getText()+ " url: "+url[j]);
                }
                /*
                * In fact, I used assert, but some of the links in the title is not included in the words so I used if-else.
                * Assert code (also sample url) is in the comments line as follows.
                * Assert.assertTrue(getWebElementWithStringXpath(driver, xpath).getText().toLowerCase().contains(url[j].toLowerCase()));
                * https://techcrunch.com/2019/05/21/how-sofar-sounds-works/
                * */
                driver.navigate().to(CONFIG.getProperty("url"));
            }
        }
    }
}
